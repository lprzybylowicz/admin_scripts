#!/bin/bash

#README
#Script is moving worklogs between issues by changing issueid value in the worklog table for a given worklog id. You can find worklogs you want to migrate by providing parameters such as source_issue_key, updated_date, author, worklog_body, time_worked. Lastly you need to provide target_issue_key and you`re ready to migrate. Before applying changes script will summarize them and ask for your confirmation.

#IMPORTANT NOTES
#- Jira should be turned off before applying any changes on a database
#- You should run the script on a test environment first
#- Script was tested on postgresql database only
#- Script doesn`t check permissions, so worklog will be migratied regardless users can work on issues or not

#required parameters
SOURCE_ISSUE_KEY="TEST-1"
#key of the issue FROM which worklogs will be exported
#example "TEST-4000" 

TARGET_ISSUE_KEY="TEST-2"
#key of the issue TO which worklogs will be imported
#example "TEST-3899"

#optional parameters
UPDATED_DATE=""
#worklogs updated date, you need to provide operator and enclose date in single quotes.
#examples: 
#updated worklog date later or equal than given date: ">='2021-02-02'"
#updated worklog date equals to given date: "='2021-02-02'"
#updated worklog date beetween to dates: "BETWEEN '2021-02-01' AND '2021-02-04'"

CREATED_DATE=""
#worklogs created date, you need to provide operator and enclose date in single quotes.
#examples: 
#created worklog date later or equal than given date: ">='2021-02-02'"
#created worklog date equals to given date: "='2021-02-02'"
#created worklog date beetween to dates: "BETWEEN '2021-02-01' AND '2021-02-04'"

START_DATE=""
#worklogs start date, you need to provide operator and enclose date in single quotes.
#examples: 
#start worklog date later or equal than given date: ">='2021-02-02'"
#start worklog date equals to given date: "='2021-02-02'"
#start worklog date beetween to dates: "BETWEEN '2021-02-01' AND '2021-02-04'"

AUTHOR=""
#login of the worklog author.
#example "lprzybyl"

WORKLOG_BODY="" 
#string that have to be in a worklog body
#examples: 
#worklog comment equals to given string: "='my_string'"
#worklog comment contains given string: "LIKE '%my_string%'"

TIME_WORKED=""
#value in seconds
#examples: 
#time spent on issue is higher than given value: ">600"
#time spent on issue is lower ir equal than given value: "<= 1200"
#time spent on issue equals to given value: "=1000"
#time spent on issue is between given value: "BETWEEN 600 and 1200"

UPDATE_REMAINING_TIME=false
#set to true if you want Remaining Time Estimate (Remaining) to be updated, default to false


issue_id() {
    local ISSUE_KEY=$1
    if [ ${#1} -gt 0 ] 
        then
            local PROJECT_KEY=$(echo "$ISSUE_KEY" | cut -d "-" -f 1)
            local ISSUE_NUM=$(echo "$ISSUE_KEY" | cut -d "-" -f 2)
            #getting target project id
            local PQUERY="SELECT id from project where pkey = '$PROJECT_KEY';"
            local PROJECT_ID=$(psql jira -XAqt -c "$PQUERY" 2> /dev/null)
                
            #getting target issue id
            local IQUERY="SELECT id from jiraissue where project = $PROJECT_ID and issuenum = $ISSUE_NUM;"
            local ISSUE_ID=$(psql jira -XAqt -c "$IQUERY" 2> /dev/null)
                
            #checking if issue with provided key existst
        if [ ${#ISSUE_ID} -eq 0 ]
            then
                echo "No issue found with key $1"
                exit 1
        fi
        echo $ISSUE_ID
    else
        echo "you need to pass TARGET_ISSUE_KEY"
        exit 1
    fi
}

#checking if the target issue was provided, if so, it needs to be translated to issue id
TARGET_ISSUE_ID="$(issue_id $TARGET_ISSUE_KEY)"
SOURCE_ISSUE_ID="$(issue_id $SOURCE_ISSUE_KEY)"

#constructing query for getting worklog ids to migrate 
PARAMS=""
PARAMS+=" worklog.issueid = $SOURCE_ISSUE_ID "
if [ ${#UPDATED_DATE} -gt 0 ] 
    then
        PARAMS+=" and worklog.updated $UPDATED_DATE "
fi

if [ ${#CREATED_DATE} -gt 0 ] 
    then
        PARAMS+=" and worklog.created $CREATED_DATE "
fi

if [ ${#START_DATE} -gt 0 ] 
    then
        PARAMS+=" and worklog.startdate $START_DATE "
fi

if [ ${#AUTHOR} -gt 0 ] 
    then
        PARAMS+=" and worklog.author = '$AUTHOR' "
fi

if [ ${#WORKLOG_BODY} -gt 0 ] 
    then
        PARAMS+=" and worklog.worklogbody $WORKLOG_BODY "
fi

if [ ${#TIME_WORKED} -gt 0 ] 
    then
        PARAMS+=" and worklog.timeworked $TIME_WORKED "
fi

#checking if there are worklogs to migrate
echo #newline
CHECK_QUERY="SELECT worklog.id from worklog INNER JOIN jiraissue on worklog.issueid = jiraissue.id INNER JOIN project on project.id = jiraissue.project where $PARAMS;"
echo $CHECK_QUERY
CHECK=$(psql jira -XAqt -c "$CHECK_QUERY")
if [ -z "$CHECK" ]
    then
        echo There are no worklogs to migrate
    exit 1
fi

#TIMEWORKED SUM IN A SOURCE ISSUE
TIMEWORKED_QUERY="SELECT worklog.timeworked from worklog INNER JOIN jiraissue on worklog.issueid = jiraissue.id where $PARAMS;"
TIMEWORKED_QUERY_EXECUTION=$(psql jira -XAqt -c "$TIMEWORKED_QUERY")
TIMEWORKED_ARR=($TIMEWORKED_QUERY_EXECUTION)
for t in "${TIMEWORKED_ARR[@]}"; do
    TIMEWORKED_SUM=$((TIMEWORKED_SUM+t))
done
echo #newline
echo TIMEWORKED in a SOURCE issue $SOURCE_ISSUE_KEY
echo "Timeworked to migrate:" $TIMEWORKED_SUM seconds
echo #newline

#TIMESPENT SUM IN A SOURCE ISSUE
TIMESPENT_SOURCE_QUERY="SELECT timespent from jiraissue where id = $SOURCE_ISSUE_ID;"
TIMESPENT_SOURCE_SUM=$(psql jira -XAqt -c "$TIMESPENT_SOURCE_QUERY")
NEW_TIMESPENT_SOURCE_SUM=$(($TIMESPENT_SOURCE_SUM-$TIMEWORKED_SUM))
echo TIMESPENT in a SOURCE issue $SOURCE_ISSUE_KEY
echo "Issue timespent before migration: $TIMESPENT_SOURCE_SUM seconds"
echo "Issue timespent after migration: $NEW_TIMESPENT_SOURCE_SUM seconds"
echo #newline

#TIMESPENT SUM IN A TARGET ISSUE
TIMESPENT_TARGET_QUERY="SELECT timespent from jiraissue where id = $TARGET_ISSUE_ID;"
TIMESPENT_TARGET_SUM=$(psql jira -XAqt -c "$TIMESPENT_TARGET_QUERY")
NEW_TIMESPENT_TARGET_SUM=$(($TIMESPENT_TARGET_SUM+$TIMEWORKED_SUM))
echo TIMESPENT in a TARGET issue $TARGET_ISSUE_KEY
echo "Issue timespent before migration: $TIMESPENT_TARGET_SUM seconds"
echo "Issue timespent after migration: $NEW_TIMESPENT_TARGET_SUM seconds"
echo #newline

if [ $UPDATE_REMAINING_TIME = true ]
    then
        TIMEESTIMATE_SOURCE_QUERY="SELECT timeestimate from jiraissue where id = $SOURCE_ISSUE_ID;"
        TIMEESTIMATE_QUERY_SOURCE_EXECUTION=$(psql jira -XAqt -c "$TIMEESTIMATE_SOURCE_QUERY")
    if [ -z "$TIMEESTIMATE_QUERY_SOURCE_EXECUTION" ]
        then
            TIMEESTIMATE_QUERY_SOURCE_EXECUTION=0
    fi
    NEW_TIMEESTIMATE_SOURCE_SUM=$(($TIMEESTIMATE_QUERY_SOURCE_EXECUTION+$TIMEWORKED_SUM))
    
    echo Remaining Time Estimate in a SOURCE issue $SOURCE_ISSUE_KEY
    echo Current time estimate $TIMEESTIMATE_QUERY_SOURCE_EXECUTION seconds
    echo New time estimate $NEW_TIMEESTIMATE_SOURCE_SUM seconds
    echo #newline
    
    TIMEESTIMATE_TARGET_QUERY="SELECT timeestimate from jiraissue where id = $TARGET_ISSUE_ID;"
    TIMEESTIMATE_QUERY_TARGET_EXECUTION=$(psql jira -XAqt -c "$TIMEESTIMATE_TARGET_QUERY")
    if [ -z "$TIMEESTIMATE_QUERY_TARGET_EXECUTION" ]
        then
            TIMEESTIMATE_QUERY_TARGET_EXECUTION=0
    fi
    
    NEW_TIMEESTIMATE_TARGET_SUM=$(($TIMEESTIMATE_QUERY_TARGET_EXECUTION-$TIMEWORKED_SUM))
    #if new time estimate is less than 0, it stays at 0 value
    if [ $NEW_TIMEESTIMATE_TARGET_SUM -lt 0 ] 
        then
            NEW_TIMEESTIMATE_TARGET_SUM=0
    fi
    
    echo Remaining Time Estimate in a TARGET issue $TARGET_ISSUE_KEY
    echo Current time estimate: $TIMEESTIMATE_QUERY_TARGET_EXECUTION seconds
    echo New time esitmate: $NEW_TIMEESTIMATE_TARGET_SUM seconds
    echo #newline
fi

#confirmation: script summarizes changes before applying them and waits for your confirmation
echo "Worklog params: $PARAMS"

TQUERY="SELECT worklog.id as worklog_id,worklog.issueid as issue_id,worklog.author as worklog_author,worklog.startdate::date as start_date,worklog.updated::date as updated_date,worklog.created::date as created_date,worklog.timeworked,concat(project.pkey, '-', jiraissue.issuenum) as issuekey from worklog INNER JOIN jiraissue on worklog.issueid = jiraissue.id INNER JOIN project on project.id = jiraissue.project where $PARAMS;"
CONFIRMATION_QUERY=$(psql jira -XAq -c "$TQUERY")
echo  #newline
echo "Following worklog entries will be moved from $SOURCE_ISSUE_KEY to $TARGET_ISSUE_KEY."
echo  #newline

#converting query to array which makes output more readable
CONF_ARR=($CONFIRMATION_QUERY)
for w in "${CONF_ARR[@]}"; do
    echo $w
done

echo  #newline

read -p "Do you want to move worklogs from $SOURCE_ISSUE_KEY to $TARGET_ISSUE_KEY (y/n)?" -n 1 -r
echo  #newline
if [[ $REPLY =~ ^[yY]$ ]]
    #getting worklogs ids after your confirmation
    then

        #updating source issue timespent
        TIMESPENT_SOURCE_UPDATE_QUERY="UPDATE jiraissue set timespent = $NEW_TIMESPENT_SOURCE_SUM where id = $SOURCE_ISSUE_ID"
        psql jira -XAqt -c "$TIMESPENT_SOURCE_UPDATE_QUERY" 2> /dev/null
        
        #updating target issue timespent
        TIMESPENT_TARGET_UPDATE_QUERY="UPDATE jiraissue set timespent = $NEW_TIMESPENT_TARGET_SUM where id = $TARGET_ISSUE_ID"
        psql jira -XAqt -c "$TIMESPENT_TARGET_UPDATE_QUERY" 2> /dev/null
    
        #updating remaining timeestimate
        if [ $UPDATE_REMAINING_TIME = true ]
            then 
                #updating target issue remaining estimate
                TIMEESTIMATE_TARGET_UPDATE_QUERY="UPDATE jiraissue set timeestimate = $NEW_TIMEESTIMATE_TARGET_SUM where id = $TARGET_ISSUE_ID"
                psql jira -XAqt -c "$TIMEESTIMATE_TARGET_UPDATE_QUERY" 2> /dev/null
                
                #updating source issue remaining estimate
                TIMEESTIMATE_SOURCE_UPDATE_QUERY="UPDATE jiraissue set timeestimate = $NEW_TIMEESTIMATE_SOURCE_SUM where id = $SOURCE_ISSUE_ID"
                psql jira -XAqt -c "$TIMEESTIMATE_SOURCE_UPDATE_QUERY" 2> /dev/null
    
        
        fi
        
        WQUERY="SELECT id from worklog where $PARAMS;"
        WORKLOG_IDS=$(psql jira -XAqt -c "$WQUERY" 2> /dev/null)
        WORKLOGS_ARR=($WORKLOG_IDS)
        
        #moving worklogs to another issue
        for w in "${WORKLOGS_ARR[@]}"; do
            UQUERY="UPDATE worklog set issueid = $TARGET_ISSUE_ID where id = $w;"
            psql jira -XAqt -c "$UQUERY" 2> /dev/null
        done
    
echo "Worklogs migration complete!"
fi
