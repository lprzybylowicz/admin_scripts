#!/bin/bash

#First, we need to specify three variables: the path of the old and new keystore and the path of the keytool. These are standard locations. The script will create a directory and files for us, where it will save the names of all certificates from both keystores. Then it will compare them, export from the old one those which are missing in the new one, and then import them into the new keystore. Then it will just clean up after itself and that’s it. Have fun. If you have any questions don`t hesitate to contact me: contact@jiraforthepeople.com.

OLD_PATH="jdk1.8.0_261/jre/lib/security/cacerts"
NEW_PATH="jre1.8.0_281/lib/security/cacerts"
KEYTOOL="jdk1.8.0_261/jre/bin/keytool"

mkdir tmpc
mkdir tmpc/certs_to_import/
touch tmpc/certs_new tmpc/certs_old tmpc/certs_new_jdk tmpc/certs_old_jdk tmpc/certs_to_export

$KEYTOOL -list -keystore $NEW_PATH > tmpc/certs_new
awk -F"[, ]" '{print $1}' tmpc/certs_new | grep -Ev '(Certificate|Keystore|Your)' | sort > tmpc/certs_new_jdk

$KEYTOOL -list -keystore $OLD_PATH > tmpc/certs_old
awk -F"[, ]" '{print $1}' tmpc/certs_old | grep -Ev '(Certificate|Keystore|Your)' | sort > tmpc/certs_old_jdk

diff --color tmpc/certs_old_jdk tmpc/certs_new_jdk | grep "<" | awk '{print $2}' > tmpc/certs_to_export

#export certs from the old keystore:
while IFS= read -r LINE; do
$KEYTOOL -export -alias "$LINE" -storepass changeit -keystore $OLD_PATH -file tmpc/certs_to_import/"$LINE".crt
done < tmpc/certs_to_export

#import certs to the new keystore:
while IFS= read -r LINE; do
$KEYTOOL -import -alias $LINE -file tmpc/certs_to_import/$LINE.crt -storepass changeit -keystore $NEW_PATH -noprompt
done < tmpc/certs_to_export

rm -rf tmpc
